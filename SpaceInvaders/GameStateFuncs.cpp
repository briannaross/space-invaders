#include <SDL_mixer.h>
#include <string>
#include "GameStateFuncs.h"

// Splash screen on program load
void GameStateIntro(GFXData &gfxData, GObjs &gObjects, Resources &resources)
{
	Timer t(1);
	t.BeginCountdown(1);
	while (t.CountdownEnded() == false)
	{
		if (!gfxData.window.IsMinimized())
		{
			SDL_RenderClear(gfxData.renderer);
			SDL_RenderCopy(gfxData.renderer, resources.intro.getTexture(), NULL, &gfxData.window.GetWindowRect());
			SDL_RenderPresent(gfxData.renderer);
		}

		t.Update();
	}

	gObjects.gGameState = eNums::GameStates::MAIN_MENU;
}

// Main menu screen
void GameStateMainMenu(SDL_Event &e, GFXData &gfxData, GObjs &gObjects, Resources &resources)
{
	while (gObjects.gGameState == eNums::GameStates::MAIN_MENU)
	{
		HandleEvent(gfxData, e, gObjects, resources);

		/*** RENDERING ***/
		if (!gfxData.window.IsMinimized())
		{
			SDL_RenderClear(gfxData.renderer);
			SDL_RenderCopy(gfxData.renderer, resources.mainmenu.getTexture(), NULL, &gfxData.window.GetWindowRect());

			// Render the space invader (menu option marker)
			SDL_Rect dst;
			dst.x = (int)(200 * gfxData.window.GetSpriteRatio());
			dst.y = (int)((170 + gObjects.gMainMenuOption * 27) * gfxData.window.GetSpriteRatio());
			dst.w = (int)(32 * gfxData.window.GetSpriteRatio());
			dst.h = (int)(20 * gfxData.window.GetSpriteRatio());

			SDL_RenderCopy(gfxData.renderer, resources.spritesheet.getTexture(), new SDL_Rect{ 0, 0, 32, 20 }, &dst);
			SDL_RenderPresent(gfxData.renderer);
		}
	}
}

// The few seconds before player is about to begin level
void GameStatePrePlay(SDL_Event &e, GFXData &gfxData, GObjs &gObjects, Resources &resources)
{
	while (gObjects.gGameState == eNums::GameStates::PRE_GAME)
	{
		gObjects.player.SetState(eNums::PlayerStates::REVIVING);

		/**** INITIALISATION ****/
		Timer PrePlayScreenTimer(1);
		PrePlayScreenTimer.BeginCountdown(3);

		while (PrePlayScreenTimer.CountdownEnded() == false)
		{
			gObjects.player.Animate();
			RenderGame(gfxData, resources, gObjects);
			PrePlayScreenTimer.Update();
		}
		gObjects.player.MakeVisible();

		gObjects.gGameState = eNums::GameStates::PLAYING_GAME;
		gObjects.player.SetState(eNums::PlayerStates::ALIVE);
	}

}

// The actual game
void GameStatePlayingGame(SDL_Event &e, GFXData &gfxData, GObjs &gObjects, Resources &resources)
{
	// Set up timer. This will determine how many times per second the game is updated.
	Timer FPSTimer(FRAMES_PER_SECOND);

	// Set random number generator to the number of aliens (to be used for missile generation).
	srand(Alien::GetNumAliens());

	/*** GAME LOOP ***/
	// Main game loop (while application is running...)
	while (gObjects.gGameState == eNums::GameStates::PLAYING_GAME || gObjects.gGameState == eNums::GameStates::PAUSED)
	{
		/**** GAME LOGIC ****/
		FPSTimer.Update();

		// Perform logic and rendering every frame
		if (FPSTimer.HasTicked())
		{
			if (gObjects.gGameState == eNums::GameStates::PLAYING_GAME)
			{
				gObjects.alienHoard.ProcessDeadAndDyingAliens();
				gObjects.motherShip.ProcessDeath();

				// Move sprites
				gObjects.motherShip.Move();
				gObjects.alienHoard.Move();
				gObjects.alienMissiles.Move();
				if (gObjects.playerMissile.HasSpawned())
					gObjects.playerMissile.Move();


				// Handle player missile
				// Player missile hits alien
				if (gObjects.playerMissile.HasSpawned())
				{
					// Player missile hits alien, increment score. 0 points implies no hit.
					int hitPoints = gObjects.alienHoard.CheckForHit(gObjects.playerMissile);
					if (hitPoints > 0)
						gObjects.player.AddScore(hitPoints);
					// Player missile hits shield
					else if (gObjects.shieldArray.Hit(gObjects.playerMissile))
						Mix_PlayChannel(-1, resources.sndExplosion, 0);
					// Player missile goes off the screen
					else if (gObjects.playerMissile.CheckIfLeftPlayfield())
						continue;
					// Player missile hits mothership
					else if (gObjects.motherShip.HasSpawned() && !gObjects.motherShip.IsDying())
						if (gObjects.playerMissile.Hit(gObjects.motherShip.GetRect()))
						{
							gObjects.motherShip.StartDying();
							Mix_PlayChannel(-1, resources.sndExplosion, 0);
							gObjects.player.AddScore(gObjects.motherShip.GetHitPoints());
						}
				}

				// TODO Add code to give player an extra life at a certain threshold

				// Handle alien missile
				// Set to true if alien missile hits player or aliens hit the ground
				bool destroyPlayer = false;
				// Check if alien missile has hit player
				if (gObjects.alienMissiles.CheckIfHit(gObjects.player.GetRect()))
					destroyPlayer = true;
				// Check if alien missile has hit the shield array
				else if (gObjects.alienMissiles.CheckIfHit(gObjects.shieldArray))
					Mix_PlayChannel(-1, resources.sndExplosion, 0);
				// Check if alien missile has left playfield
				else if (gObjects.alienMissiles.CheckIfLeftPlayfield())
					continue;

				// *** Alien attack ***
				// Random alien fires missile
				gObjects.alienHoard.FireMissile(gObjects.alienMissiles);

				// *** Player death ***
				// Hoard has landed; kill the player and end the game
				if (gObjects.alienHoard.Landed())
				{
					while (gObjects.player.GetLives() > 0)
						gObjects.player.Destroy();

					gObjects.gGameState = eNums::GameStates::PLAYER_LOST_LIFE;
				}
				// Destroy player
				else if (destroyPlayer == true)
				{
					Mix_PlayChannel(-1, resources.sndDeath, 0);
					gObjects.player.Destroy();
					gObjects.gGameState = eNums::GameStates::PLAYER_LOST_LIFE;

					destroyPlayer = false;
				}
			}
		}
		
		/**** EVENT HANDLING ****/
		HandleEvent(gfxData, e, gObjects, resources);

		/*** RENDERING ***/
		RenderGame(gfxData, resources, gObjects);
	}
}

void GameStatePlayerLostLife(SDL_Event &e, GFXData &gfxData, GObjs &gObjects, Resources &resources)
{
	while (gObjects.gGameState == eNums::GameStates::PLAYER_LOST_LIFE)
	{
		gObjects.player.SetState(eNums::PlayerStates::DYING);

		/**** INITIALISATION ****/
		Timer LostLifeScreenTimer(1);
		LostLifeScreenTimer.BeginCountdown(3);

		while (LostLifeScreenTimer.CountdownEnded() == false)
		{
			gObjects.player.Animate();

			RenderGame(gfxData, resources, gObjects);

			LostLifeScreenTimer.Update();
		}

		if (gObjects.player.GetLives() > 0)
		{
			gObjects.player.ResetPosition();
			gObjects.motherShip.Reset();
			gObjects.playerMissile.Reset();
			gObjects.alienMissiles.Reset();
			gObjects.gGameState = eNums::GameStates::PRE_GAME;
			gObjects.player.SetState(eNums::PlayerStates::REVIVING);
		}
		else
		{
			gObjects.gGameState = eNums::GameStates::END_GAME;
			gObjects.player.SetState(eNums::PlayerStates::DEAD);
		}
	}
}

void GameStateEndGame(SDL_Event &e, GFXData &gfxData, GObjs &gObjects, Resources &resources)
{
	//TODO Implement functionality - probably just a splash screen or some such thing
	gObjects.gGameState = eNums::GameStates::MAIN_MENU;


	// Convert score string literal to integer
	int lowestHighScore = 0;
	for (int i = 0; i < 9; i++)
	{
		if (gObjects.highScores[9].score[i] >= '0' && gObjects.highScores[9].score[i] <= '9')
		{
			int indexVal;
			switch (gObjects.highScores[9].score[i])
			{
			case '1':	indexVal = 1;	break;
			case '2':	indexVal = 2;	break;
			case '3':	indexVal = 3;	break;
			case '4':	indexVal = 4;	break;
			case '5':	indexVal = 5;	break;
			case '6':	indexVal = 6;	break;
			case '7':	indexVal = 7;	break;
			case '8':	indexVal = 8;	break;
			case '9':	indexVal = 9;	break;
			case '0':	indexVal = 0;	break;
			}
			lowestHighScore *= 10;
			lowestHighScore += indexVal;
		}
	}


	if (gObjects.player.GetScore() > lowestHighScore)
	{
		// TODO Play fanfare

		int initials_index = 0;
		std::string initials = "";

		while (gObjects.gGameState == eNums::GameStates::END_GAME)
		{
		/*
		print message stating player has achieved a high score
		print player score

		prompt to enter initial
		if player hits a backspace or delete
		delete initials[initials_index]
		if initials_index is greater than 0
		initial_index--
		else
		if player hits an 'approved' key
		initials[initials_index] = player_key
		if initials_index is less than 2
		initial_index++

		else
		if player hits enter
		gamestate = highscorescreen

		render screen
		*/
		}
	}

}

void GameStateHighScores(SDL_Event &e, GFXData &gfxData, GObjs &gObjects, Resources &resources)
{
	//TODO Implement functionality - prompt player to enter name if score high enough for high score chart
	//TODO Move this stuff into the gObjects struct

	while (gObjects.gGameState == eNums::GameStates::HIGH_SCORES)
	{
		HandleEvent(gfxData, e, gObjects, resources);

		/*** RENDERING ***/
		if (!gfxData.window.IsMinimized())
		{
			SDL_RenderClear(gfxData.renderer);

			//TODO Change this to another background (maybe).
			SDL_RenderCopy(gfxData.renderer, resources.background.getTexture(), NULL, &gfxData.window.GetWindowRect());

			for (int i = 0; i < 10; i++)
			{
				// Render high scores
				resources.mainFont.setText(gfxData, gObjects.highScores[i].initials);
					
				SDL_Rect dst;

				dst.x = (int)(200 * gfxData.window.GetSpriteRatio());
				dst.y = (int)(50 + (i * 30) * gfxData.window.GetSpriteRatio());
				dst.h = (int)(resources.mainFont.getHeight() * gfxData.window.GetSpriteRatio());
				dst.w = (int)(resources.mainFont.getWidth() * gfxData.window.GetSpriteRatio());

				SDL_RenderCopy(gfxData.renderer, resources.mainFont.getTexture(), NULL, &dst);

				resources.mainFont.setText(gfxData, gObjects.highScores[i].score);

				dst.x = (int)(400 * gfxData.window.GetSpriteRatio());
				dst.y = (int)(50 + (i * 30) * gfxData.window.GetSpriteRatio());
				dst.h = (int)(resources.mainFont.getHeight() * gfxData.window.GetSpriteRatio());
				dst.w = (int)(resources.mainFont.getWidth() * gfxData.window.GetSpriteRatio());

				SDL_RenderCopy(gfxData.renderer, resources.mainFont.getTexture(), NULL, &dst);
			}

			SDL_RenderPresent(gfxData.renderer);
		}
	}
}

void GameStateOuttro(GFXData &gfxData, GObjs &gObjects, Resources &resources)
{
	Timer t(1);
	t.BeginCountdown(3);
	while (t.CountdownEnded() == false)
	{
		if (!gfxData.window.IsMinimized())
		{
			SDL_RenderClear(gfxData.renderer);
			SDL_RenderCopy(gfxData.renderer, resources.outtro.getTexture(), NULL, &gfxData.window.GetWindowRect());
			SDL_RenderPresent(gfxData.renderer);
		}

		t.Update();
	}

	gObjects.gGameState = eNums::GameStates::END_GAME;
}