#pragma once

#include <string>
#include <SDL.h>
#include "GFXData.h"
#include "Texture.h"

class Font
{
public:
	Font();
	~Font();
	bool setFont(GFXData &myGFXData, std::string fontFile);
	bool setText(GFXData &myGFXData, std::string text);
	bool setColour(GFXData &myGFXData, SDL_Color colour);
	SDL_Texture* getTexture();
	int getHeight();
	int getWidth();

private:
	TTF_Font *mFont;
	Texture mTextTexture;
	SDL_Color mTextColour;
	std::string mTextString;
};
