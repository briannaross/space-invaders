#pragma once

#include <SDL.h>
#include "Texture.h"
#include "Timer.h"
#include "Window.h"

class BGameObject
{
public:
	BGameObject();
	~BGameObject();

	void StartDying();
	bool IsDying();
	void ProcessDying();
	bool IsDead();

	SDL_Rect GetRect();
	void Render(SDL_Renderer *renderer, Texture &texture);

	static void SetRatios(float ratio, LF_Window &window);

protected:
	static float smSpriteRatio;
	static int smXOffset;
	static int smYOffset;
	int mAnimFrameOffset;

	bool mDying;
	bool mDead;
	int mTicksUntilDeath;
	int mDeathTicksPassed;

	SDL_Rect mSrc;
	SDL_Rect mDst;
	Timer *mTimer;
};
