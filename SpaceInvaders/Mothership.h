#pragma once

#include <SDL.h>
#include "BGameObject.h"
#include "Texture.h"
#include "Timer.h"

class Mothership : public BGameObject
{
public:
	Mothership();
	~Mothership();

	void Spawn();
	bool HasSpawned();
	int GetHitPoints();
	void Move();
	void Animate();
	bool CheckForOffPlayfield();
	void ProcessDeath();
	void Destroy();
	void Reset();

private:
	bool mSpawned;
	bool mMoveLeft = true;
	int mSpriteFrame;
	int mHitPoints;
	int mTimeUntilRespawn;
	double mXPos;
	SDL_Point mSpritePositions[2];
	//Timer *mTimer;

	//TODO Set a random velocity of 1 or -1 and create code that can can randomly move the mothership from left of screen to right of screen or right of screen to left of screen
};
