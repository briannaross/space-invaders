#pragma once

#include <SDL.h>
#include "BGameObject.h"
#include "Texture.h"

class Shield : public BGameObject
{
public:
	Shield(int row, int column, SDL_Point pos);
	~Shield();
	SDL_Point GetPos();
	SDL_Rect GetRect();
	int GetRow();
	int GetColumn();
	void Damage();
	bool Destroyed();
	void UpdateSpriteSourcePos();

private:
	int mColumn;
	int mRow;
	SDL_Point mPos;
	int mHitPoints;
};
