#pragma once

#include <SDL.h>
#include <SDL_mixer.h>
#include "BGameObject.h"
#include "Texture.h"
#include "Globals.h"

class PlayerMissile : public BGameObject
{
public:
	PlayerMissile();
	~PlayerMissile();
	bool Fire(SDL_Rect &spawnRect, Mix_Chunk *snd);
	void Move();
	bool Hit(SDL_Rect target);
	bool CheckIfLeftPlayfield();
	bool HasSpawned();
	void Reset();

private:
	void ResetPosition();
		
	int mVelocity;
	double mYPos;
	SDL_Point mPos;
	bool mSpawned;
};
