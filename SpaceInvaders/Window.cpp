#include "Window.h"
#include "Globals.h"

LF_Window::LF_Window()
{
	//Initialize non-existant window
	mWindow = NULL;
	mMouseFocus = false;
	mKeyboardFocus = false;
	mFullScreen = false;
	mMinimized = false;
	mWidth = 0;
	mHeight = 0;
	mSpriteRatio = 1.0f;

}

bool LF_Window::Init()
{
	//Create window
	mWindow = SDL_CreateWindow("My Space Invaders Clone", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, INIT_SCREEN_WIDTH, INIT_SCREEN_HEIGHT, SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE);
	if (mWindow != NULL)
	{
		mMouseFocus = true;
		mKeyboardFocus = true;
		mWidth = INIT_SCREEN_WIDTH;
		mHeight = INIT_SCREEN_HEIGHT;
		mInitScreenRatio = INIT_SCREEN_WIDTH / INIT_SCREEN_HEIGHT;
	}

	return mWindow != NULL;
}

SDL_Renderer* LF_Window::CreateRenderer()
{
	return SDL_CreateRenderer(mWindow, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
}

void LF_Window::HandleEvent(SDL_Event& e, SDL_Renderer *gRenderer)
{
	//Window event occured
	if (e.type == SDL_WINDOWEVENT)
	{
		//Caption update flag
		bool updateCaption = false;

		switch (e.window.event)
		{
			//Get new dimensions and repaint on window size change
		case SDL_WINDOWEVENT_SIZE_CHANGED:
			mWidth = e.window.data1;
			mHeight = e.window.data2;
			SDL_RenderPresent(gRenderer);
			SetSpriteRatio();
			break;

			//Repaint on exposure
		case SDL_WINDOWEVENT_EXPOSED:
			SDL_RenderPresent(gRenderer);
			break;

			//Mouse entered window
		case SDL_WINDOWEVENT_ENTER:
			mMouseFocus = true;
			updateCaption = true;
			break;

			//Mouse left window
		case SDL_WINDOWEVENT_LEAVE:
			mMouseFocus = false;
			updateCaption = true;
			break;

			//Window has keyboard focus
		case SDL_WINDOWEVENT_FOCUS_GAINED:
			mKeyboardFocus = true;
			updateCaption = true;
			break;

			//Window lost keyboard focus
		case SDL_WINDOWEVENT_FOCUS_LOST:
			mKeyboardFocus = false;
			updateCaption = true;
			break;

			//Window minimized
		case SDL_WINDOWEVENT_MINIMIZED:
			mMinimized = true;
			break;

			//Window maxized
		case SDL_WINDOWEVENT_MAXIMIZED:
			mMinimized = false;
			break;

			//Window restored
		case SDL_WINDOWEVENT_RESTORED:
			mMinimized = false;
			break;
		}
	}
	//Enter exit full screen on return key
	else if (e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_F12)
	{
		if (mFullScreen)
		{
			SDL_SetWindowFullscreen(mWindow, SDL_FALSE);
			mFullScreen = false;
		}
		else
		{
			SDL_SetWindowFullscreen(mWindow, SDL_TRUE);
			mFullScreen = true;
			mMinimized = false;
		}
	}
}

void LF_Window::Free()
{
	if (mWindow != NULL)
		SDL_DestroyWindow(mWindow);

	mMouseFocus = false;
	mKeyboardFocus = false;
	mWidth = 0;
	mHeight = 0;
}

int LF_Window::GetWidth()
{
	return mWidth;
}

int LF_Window::GetHeight()
{
	return mHeight;
}

float LF_Window::GetSpriteRatio()
{
	return mSpriteRatio;
}

int LF_Window::getXBuffer()
{
	return mXBuffer;
}

int LF_Window::getYBuffer()
{
	return mYBuffer;
}

void LF_Window::SetSpriteRatio()
{
	float currentScreenRatio = ((float)mWidth / (float)mHeight);

	if (currentScreenRatio > mInitScreenRatio)
		mSpriteRatio = (float)mHeight / (float)INIT_SCREEN_HEIGHT;
	else
		mSpriteRatio = (float)mWidth / (float)INIT_SCREEN_WIDTH;

	mXBuffer = (int)((mWidth - (INIT_SCREEN_WIDTH * mSpriteRatio)) / 2);
	mYBuffer = (int)((mHeight - (INIT_SCREEN_HEIGHT * mSpriteRatio)) / 2);
}

SDL_Rect LF_Window::GetWindowRect()
{
	return SDL_Rect{ 0, 0, mWidth, mHeight };
}

bool LF_Window::HasMouseFocus()
{
	return mMouseFocus;
}

bool LF_Window::HasKeyboardFocus()
{
	return mKeyboardFocus;
}

bool LF_Window::IsMinimized()
{
	return mMinimized;
}
