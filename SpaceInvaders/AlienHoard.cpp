#include <iostream>
#include "AlienHoard.h"

AlienHoard::AlienHoard()
{
	Reset();

	timer = new Timer(1);
}


AlienHoard::~AlienHoard()
{
	delete timer;
	timer = nullptr;
}

void AlienHoard::Move()
{
	// Initialise timer interval...
	SetMovementInterval();

	// ...then update the timer.
	timer->Update();

	if (timer->HasTicked()) {
		bool animateAlien = false;

		// Check whether any alien has hit the playfield boundary. If so mark
		// them for moving down a row.
		bool moveDown = CollidedWithEdge();

		for (auto &itAlien : hoard) {
			// Do death animation, with explody-type bits and shit.
			if (!itAlien.IsDying()) {
				itAlien.Animate();
			}

			if (moveDown == true) {
				itAlien.MoveDown();
			}
			else {
				itAlien.MoveAcross();
			}
		}
	}

}

void AlienHoard::SetMovementInterval()
{
	switch (Alien::GetNumAliens()) {
		case 48:	timer->SetTicksPerSecond(1);	break;
		case 24:	timer->SetTicksPerSecond(2);	break;
		case 12:	timer->SetTicksPerSecond(4);	break;
		case 6:		timer->SetTicksPerSecond(8);	break;
		case 3:		timer->SetTicksPerSecond(16);	break;
		case 1:		timer->SetTicksPerSecond(32);	break;
		//default:	alienTimer.SetTicksPerSecond(1);	break;
	}
}

bool AlienHoard::CollidedWithEdge()
{
	if (hoard.size() > 0) {
		// Check the leftmost alien
		if (hoard.begin()->CollidedWithEdge())	return true;
		// Check the rightmost alien
		if (hoard.back().CollidedWithEdge())	return true;
	}

	return false;
}

void AlienHoard::Reset()
{
	// Clear all the current aliens (if any)
	for (auto &itAlien : hoard) {
		itAlien.Destroy();
	}

	hoard.clear();

	// Populate the hoard with aliens. Do it column by column, because
	// this will allow us to check the first and last elements of the hoard
	// for moving down, thus eliminating the need to process the whole vector.
	for (int column = 0; column < NUM_ALIEN_COLUMNS; column++) {
		for (int row = 0; row < NUM_ALIEN_ROWS; row++) {
			hoard.push_back(Alien(row, column));
		}
	}
}

void AlienHoard::Render(SDL_Renderer *renderer, Texture &texture)
{
	for (auto &itAlien : hoard) {
		itAlien.Render(renderer, texture);
	}
}

void AlienHoard::FireMissile(AlienMissiles &alienMissiles)
{
	for (auto &itAlien : hoard) {
		if (!itAlien.IsDying()) {
			// Randomly fire an alien missile, provided there are no more than two on screen.
			if (AlienMissile::GetNumMissiles() < MAX_ALIEN_MISSILES && rand() % Alien::GetNumAliens() == 1) {
				SDL_Rect alienRect = itAlien.GetRect();
				SDL_Point origin;
				origin.x = alienRect.x + (alienRect.w / 2);
				origin.y = alienRect.y;
				alienMissiles.Spawn(origin);
				// Once a missile is fired we don't need to fire another one....yet!
				return;
			}
		}
	}

	return;
}

int AlienHoard::CheckForHit(PlayerMissile &playerMissile)
{
	for (auto &itAlien : hoard) {
		if (!itAlien.IsDying()) {
			// Check if missile has hit alien. If so, add score to player score,
			// destroy alien, and erase alien from vector.
			if (playerMissile.Hit(itAlien.GetRect())) {
				int hitPoints = itAlien.GetHitPoints();
				itAlien.StartDying();
				// Only one alien can be hit, so return the score for destroying that alien.
				return hitPoints;
			}
		}
	}

	return 0;
}

void AlienHoard::ProcessDeadAndDyingAliens()
{
	itAlien = hoard.begin();
	while (itAlien != hoard.end()) {
		if (itAlien->IsDying()) {
			itAlien->ProcessDying();

			// Alien is removed from vector once dead. We don't increment the iterator
			// as because we need to process the alien that takes it's position in the vector.
			if (itAlien->IsDead()) {
				itAlien->Destroy();
				itAlien = hoard.erase(itAlien);

				// Set random number generator based on number of aliens left.
				// This affects alien missile generation.
				srand(Alien::GetNumAliens());
			}
			else
				itAlien++;
		}
		else
			itAlien++;
	}
}

bool AlienHoard::Landed()
{
	for (itAlien = hoard.begin(); itAlien != hoard.end(); ) {
		if (itAlien->IsDying()) {
			// Check if aliens have hit the ground.
			if (itAlien->GetRect().y >= HOARD_LANDED_Y_POSITION) {
				// Destroy all aliens because, well, game is over!
				itAlien->Destroy();
				itAlien = hoard.erase(itAlien);
				return true;
			}
			else
				itAlien++;
		}
		else
			itAlien++;
	}

	return false;
}
