#include "AlienMissiles.h"
#include "Globals.h"


AlienMissiles::AlienMissiles()
{
}


AlienMissiles::~AlienMissiles()
{
}

void AlienMissiles::Move()
{
	for (itAlienMissile = mAlienMissiles.begin(); itAlienMissile != mAlienMissiles.end(); itAlienMissile++)
		itAlienMissile->Move();
}

void AlienMissiles::Spawn(SDL_Point &origin)
{
	mAlienMissiles.push_back(AlienMissile(origin));
}


bool AlienMissiles::CheckIfHit(SDL_Rect &target)
{
	for (itAlienMissile = mAlienMissiles.begin(); itAlienMissile != mAlienMissiles.end(); itAlienMissile++)
	{
		// Alien missile hits player
		if (itAlienMissile->Hit(target))
		{
			itAlienMissile->Destroy();
			itAlienMissile = mAlienMissiles.erase(itAlienMissile);
			return true;
		}
	}

	return false;
}

bool AlienMissiles::CheckIfHit(ShieldArray &shieldArray)
{
	for (itAlienMissile = mAlienMissiles.begin(); itAlienMissile != mAlienMissiles.end(); itAlienMissile++)
	{
		// Alien missile hits shield
		if (shieldArray.Hit(itAlienMissile))
		{
			itAlienMissile->Destroy();
			itAlienMissile = mAlienMissiles.erase(itAlienMissile);
			return true;
		}
	}

	return false;
}


bool AlienMissiles::CheckIfLeftPlayfield()
{
	for (itAlienMissile = mAlienMissiles.begin(); itAlienMissile != mAlienMissiles.end(); itAlienMissile++)
	{
		if (itAlienMissile->LeftPlayfield())
		{
			itAlienMissile->Destroy();
			itAlienMissile = mAlienMissiles.erase(itAlienMissile);
			return true;
		}
	}

	return false;
}

void AlienMissiles::Reset()
{
	for (itAlienMissile = mAlienMissiles.begin(); itAlienMissile != mAlienMissiles.end(); itAlienMissile++)
		itAlienMissile->Destroy();
	mAlienMissiles.clear();
}

void AlienMissiles::Render(SDL_Renderer *renderer, Texture &texture)
{
	for (itAlienMissile = mAlienMissiles.begin(); itAlienMissile != mAlienMissiles.end(); itAlienMissile++)
		itAlienMissile->Render(renderer, texture);
}
