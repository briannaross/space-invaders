#pragma once

#include <SDL.h>
#include "Structs.h"
#include "Globals.h"
#include "RenderFuncs.h"
#include "Timer.h"

void GameStateIntro(GFXData &gfxData, GObjs &gObjects, Resources &resources);
void GameStateMainMenu(SDL_Event &e, GFXData &gfxData, GObjs &GObjects, Resources &resources);
void GameStatePrePlay(SDL_Event &e, GFXData &gfxData, GObjs &gObjects, Resources &resources);
void GameStatePlayingGame(SDL_Event &e, GFXData &gfxData, GObjs &gObjects, Resources &resources);
void GameStatePlayerLostLife(SDL_Event &e, GFXData &gfxData, GObjs &gObjects, Resources &resources);
void GameStateEndGame(SDL_Event &e, GFXData &gfxData, GObjs &gObjects, Resources &resources);
void GameStateHighScores(SDL_Event &e, GFXData &gfxData, GObjs &gObjects, Resources &resources);
void GameStateOuttro(GFXData &gfxData, GObjs &gObjects, Resources &resources);
