#include "Player.h"
#include "ENums.h"

#include <SDL.h>

Player::Player(std::string name)
{
	// Initial frame on spritesheet 
	mSrc.x = 0;
	mSrc.y = 140;
	mSrc.h = 20;
	mSrc.w = 28;

	Reset();
}

Player::~Player()
{
	delete mTimer;
	mTimer = nullptr;
}

void Player::HandleEvents(SDL_Event* e)
{
	const Uint8 *state = SDL_GetKeyboardState(NULL);

	mVelocity = 0;

	if (state[SDL_SCANCODE_LEFT])
		mVelocity = -1;
	else if (state[SDL_SCANCODE_RIGHT])
		mVelocity = 1;

}

void Player::Move(eNums::Direction direction)
{
	mDst.x += (direction * PLAYER_SPEED);

	CheckCollision();
	Animate();
}

void Player::Animate()
{
	mTimer->Update();
	if (mTimer->HasTicked())
		mSpriteFrame = (mSpriteFrame + 1) % 2;
	{
		//TODO Add animations for each state
		if (mState == eNums::PlayerStates::ALIVE)
		{
			mSrc.x = 0;
		}
		else if (mState == eNums::PlayerStates::REVIVING)
		{
			if (mSpriteFrame == 0)
				mSrc.x = 0;
			else
				// Currently blank, so this will make the sprite look like it's flashing, like on the 2600.
				mSrc.x = 64;
		}
		else if (mState == eNums::PlayerStates::DYING)
		{
			if (mSpriteFrame == 0)
				mSrc.x = 0;
			else
				mSrc.x = 32;
		}
		else if (mState == eNums::PlayerStates::DEAD)
		{
			// Animate
		}
	}
}

void Player::Destroy()
{
	mLives--;
}

void Player::AddScore(int scored)
{
	mScore += scored;
}

int Player::GetScore()
{
	return mScore;
}

int Player::GetLives()
{
	return mLives;
}

void Player::SetState(eNums::PlayerStates playerState)
{
	mState = playerState;
}

void Player::Reset()
{
	mLives = 3;
	mScore = 0;
	mVelocity = 0;
	mSpriteFrame = 0;
	mTimer = new Timer(4);

	// Rendering position
	ResetPosition();

	mState = eNums::PlayerStates::ALIVE;
}

void Player::ResetPosition()
{
	mDst.x = 304;
	mDst.y = 450;
	mDst.h = 20;
	mDst.w = 28;
}

void Player::CheckCollision()
{
	if (mDst.x <= INIT_SIDE_BUFFER)
		mDst.x = INIT_SIDE_BUFFER;
	else if (mDst.x > INIT_SCREEN_WIDTH - mDst.w - INIT_SIDE_BUFFER)
		mDst.x = INIT_SCREEN_WIDTH - mDst.w - INIT_SIDE_BUFFER;
}

void Player::MakeVisible()
{
	mSpriteFrame = 0;
	mSrc.x = 0;
}