#include "ShieldArray.h"
#include "Globals.h"

ShieldArray::ShieldArray()
{
	Reset();
}


ShieldArray::~ShieldArray()
{
}

void ShieldArray::Reset()
{
	shieldArray.clear();

	for (int shieldNum = 0; shieldNum < NUM_SHIELDS; shieldNum++)
	{
		std::vector<Shield> shield;
		SDL_Point sdlp;
		for (int segment = 0; segment < NUM_SHIELD_SEGMENTS; segment++)
		{
			int row = segment / 4;
			int column = segment % 4;
			sdlp.x = (161 + (shieldNum * 140));
			sdlp.y = 400;
			shield.push_back(Shield(row, column, sdlp));
		}
		shieldArray.push_back(shield);
	}

}

bool ShieldArray::Hit(PlayerMissile &playerMissile)
{
	for (shield = shieldArray.begin(); shield != shieldArray.end(); shield++)
		for (shieldSegment = shield->begin(); shieldSegment != shield->end(); shieldSegment++)
			if (playerMissile.Hit(shieldSegment->GetRect()))
			{
				shieldSegment->Damage();
				if (shieldSegment->Destroyed())
					shieldSegment = shield->erase(shieldSegment);

				return true;
			}

	return false;

}

bool ShieldArray::Hit(std::vector<AlienMissile>::iterator &alienMissile)
{
	for (shield = shieldArray.begin(); shield != shieldArray.end(); shield++)
		for (shieldSegment = shield->begin(); shieldSegment != shield->end(); shieldSegment++)
			if (alienMissile->Hit(shieldSegment->GetRect()))
			{
				shieldSegment->Damage();
				if (shieldSegment->Destroyed())
					shieldSegment = shield->erase(shieldSegment);

				return true;
			}

	return false;
}

void ShieldArray::Render(SDL_Renderer *renderer, Texture &texture)
{
	for (shield = shieldArray.begin(); shield != shieldArray.end(); shield++)
		for (shieldSegment = shield->begin(); shieldSegment != shield->end(); shieldSegment++)
			shieldSegment->Render(renderer, texture);
}
