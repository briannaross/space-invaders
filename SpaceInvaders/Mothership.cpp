#include "Globals.h"
#include "Mothership.h"

Mothership::Mothership()
{
	mTimer = new Timer(1);

	Reset();

	mHitPoints = 500;

	mSpritePositions[0].x = 0;
	mSpritePositions[0].y = 122;
	mSpritePositions[1].x = 28;
	mSpritePositions[1].y = 122;

	// Position of mothership image on spritesheet
	mSrc.x = mSpritePositions[0].x;
	mSrc.y = mSpritePositions[0].y;
	mSrc.h = 16;
	mSrc.w = 28;

	mAnimFrameOffset = 28;
	
	mTicksUntilDeath = 5;

	mSpriteFrame = 0;
}

Mothership::~Mothership()
{
}

void Mothership::Spawn()
{
	mSpawned = true;
}

bool Mothership::HasSpawned()
{
	return mSpawned;
}

int Mothership::GetHitPoints()
{
	return mHitPoints;
}

void Mothership::Move()
{
	if (!IsDying())
	{
		mTimer->Update();

		if (mSpawned == true)
		{
			Animate();
			mXPos -= 1.0f;
			mDst.x = (int)mXPos;
			if (CheckForOffPlayfield())
				Destroy();
		}
		else
			if (mTimer->HasTicked())
			{
				mTimeUntilRespawn--;
				if (mTimeUntilRespawn <= 0)
					Spawn();
			}
	}
}

void Mothership::Animate()
{
	mSpriteFrame = (mSpriteFrame + 1) % 2;
	mSrc.x = mSpritePositions[mSpriteFrame].x;
	mSrc.y = mSpritePositions[mSpriteFrame].y;
}

bool Mothership::CheckForOffPlayfield()
{
	// Only check if the ship has left the left side of the screen.
	if (mDst.x + mDst.w < 0)
		return true;

	return false;
}

void Mothership::ProcessDeath()
{
	if (IsDying())
	{
		ProcessDying();
		if (IsDead())
			Destroy();
	}
}

void Mothership::Destroy()
{
	Reset();
}

void Mothership::Reset()
{
	mMoveLeft = true;
	mSpawned = false;
	mSpriteFrame = 0;
	mTimeUntilRespawn = MOTHERSHIP_RESPAWN;
	mXPos = INIT_SCREEN_WIDTH;

	mDst.x = (int)mXPos;
	mDst.y = 50;
	mDst.h = 20;
	mDst.w = 32;

	mSrc.x = 0; // Reset anim frame

	mDying = false;
	mDead = false;

	mDeathTicksPassed = 0;

	mTimer->SetTicksPerSecond(1);
}
