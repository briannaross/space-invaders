#pragma once

#include <SDL.h>
#include "ENums.h"

const int INIT_SCREEN_WIDTH = 640;
const int INIT_SCREEN_HEIGHT = 480;
const int INIT_SIDE_BUFFER = 45;
const int MISSILE_SPEED = 6;
const int PLAYER_SPEED = 5;
const int MOTHERSHIP_RESPAWN = 15;
const int FRAMES_PER_SECOND = 60;
const int NUM_ALIEN_ROWS = 6;
const int NUM_ALIEN_COLUMNS = 8;
const int NUM_SHIELDS = 3;
const int NUM_SHIELD_SEGMENTS = 12;
const int MAX_ALIEN_MISSILES = 3;
const int PLAYER_MISSILE_INTERVAL = 2;
const int HOARD_LANDED_Y_POSITION = 400;
const int SHIELD_HIT_POINTS = 4; // Num of hits shield section can take before being destroyed
