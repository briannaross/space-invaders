#pragma once

#include <string>
#include "BGameObject.h"
#include "Globals.h"
#include "ENums.h"
#include "Texture.h"
#include "Timer.h"

class Player : public BGameObject
{
public:
	Player(std::string name);
	~Player();
	void HandleEvents(SDL_Event* e);
	void Move(eNums::Direction direction);
	void Animate();
	void AddScore(int scored);
	void Destroy();
	int GetScore();
	int GetLives();
	void SetState(eNums::PlayerStates playerState);
	void Reset();
	void ResetPosition();
	void MakeVisible();

private:
	void CheckCollision();

	int mLives;
	int mScore;
	int mVelocity;
	int mSpriteFrame;
	eNums::PlayerStates mState;
	//Timer *mAnimTimer;
};
