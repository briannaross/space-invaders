#pragma once

#include <SDL.h>
#include "BGameObject.h"
#include "Globals.h"
#include "Texture.h"

class AlienMissile : public BGameObject
{
public:
	AlienMissile(SDL_Point &origin);
	~AlienMissile();
	void Move();
	bool Hit(SDL_Rect target);
	bool LeftPlayfield();
	void Destroy();
	static int GetNumMissiles();

private:
	int mVelocity;
	double mYPos;
	SDL_Point mPos;
	static int smNumMissiles;
};
