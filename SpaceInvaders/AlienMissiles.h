#pragma once

#include <vector>
#include <SDL.h>
#include "AlienMissile.h"
#include "ShieldArray.h"
#include "Texture.h"

class AlienMissiles
{
public:
	AlienMissiles();
	~AlienMissiles();
	void Move();
	void Spawn(SDL_Point &origin);
	bool CheckIfHit(SDL_Rect &target);
	bool CheckIfHit(ShieldArray &shieldArray);
	bool CheckIfLeftPlayfield();
	void Reset();
	void Render(SDL_Renderer *renderer, Texture &texture);

private:
	std::vector<AlienMissile> mAlienMissiles;
	std::vector<AlienMissile>::iterator itAlienMissile;
};
