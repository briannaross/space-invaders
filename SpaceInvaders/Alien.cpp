#include <iostream>
#include "Globals.h"
#include "Alien.h"

int Alien::smNumAliens = 0;

Alien::Alien(int row, int column)
{
	switch (row) {

		case 0: // Bottom row
			mSpritePositions[0].x = 0;
			mSpritePositions[0].y = 100;
			mSpritePositions[1].x = 32;
			mSpritePositions[1].y = 100;
			break;
		case 1:
			mSpritePositions[0].x = 0;
			mSpritePositions[0].y = 80;
			mSpritePositions[1].x = 32;
			mSpritePositions[1].y = 80;
			break;
		case 2:
			mSpritePositions[0].x = 0;
			mSpritePositions[0].y = 60;
			mSpritePositions[1].x = 32;
			mSpritePositions[1].y = 60;
			break;
		case 3:
			mSpritePositions[0].x = 0;
			mSpritePositions[0].y = 40;
			mSpritePositions[1].x = 32;
			mSpritePositions[1].y = 40;
			break;
		case 4:
			mSpritePositions[0].x = 0;
			mSpritePositions[0].y = 20;
			mSpritePositions[1].x = 32;
			mSpritePositions[1].y = 20;
			break;
		case 5: // Top row
			mSpritePositions[0].x = 0;
			mSpritePositions[0].y = 0;
			mSpritePositions[1].x = 32;
			mSpritePositions[1].y = 0;
			break;
		default:
			std::cout << "Error: Must specify a range of 1 to 6 for alien constructor!!!" << std::endl;
			exit(1);
	}

	// Score 10 points for bottom row, 10 additional points for each row higher
	mHitPoints = (row + 1) * 10;

	// Initial animation frame
	mSpriteFrame = 0;

	mSrc.x = mSpritePositions[mSpriteFrame].x;
	mSrc.y = mSpritePositions[mSpriteFrame].y;
	mSrc.h = 20;
	mSrc.w = 32;

	// Position of the alien
	mDst.x = 165 + (column * 40) ;
	mDst.y = 80 + ((5 - row) * 40);
	mDst.h = 20;
	mDst.w = 32;

	mTicksUntilDeath = 5;
	mDeathTicksPassed = 0;

	mDying = false;
	mDead = false;

	mAnimFrameOffset = 32;

	mTimer = new Timer(12);

	smNumAliens++;
}


Alien::~Alien()
{
	//delete mTimer;
	//mTimer = nullptr;
}

int Alien::GetHitPoints()
{
	return mHitPoints;
}

void Alien::MoveAcross()
{
	if (mMoveLeft == true)
		mDst.x -= 20;
	else
		mDst.x += 20;
}

void Alien::MoveDown()
{
	// Move down the screen
	mDst.y += 20;

	mLastTurnMovedDown = true;

	// Reverse direction
	if (mMoveLeft == true)
		mMoveLeft = false;
	else
		mMoveLeft = true;
}

void Alien::Animate()
{
	mSpriteFrame = (mSpriteFrame + 1) % 2;
	mSrc.x = mSpritePositions[mSpriteFrame].x;
	mSrc.y = mSpritePositions[mSpriteFrame].y;
}

bool Alien::CollidedWithEdge()
{
	if (mLastTurnMovedDown == true)
		mLastTurnMovedDown = false;
	else
	{
		// TODO Set these to constants
		int BUFFER = 45;

		if ((mDst.x <= BUFFER) || (mDst.x > INIT_SCREEN_WIDTH - mDst.w - BUFFER))
			return true;
	}

	return false;
}

//void Alien::StartDying()
//{
// 	mDying = true;
//	mSrc.x = 64; // Set anim frame to first one with a score
//}
//
//bool Alien::IsDying()
//{
//	return mDying;
//}
//
//void Alien::ProcessDying()
//{
//	mTimer->Update();
//	if (mTimer->HasTicked())
//	{
//		if (mDeathTicksPassed < mTicksUntilDeath )
//		{
//			mDeathTicksPassed++;
//			mSrc.x += 32; // Set next death anim frame
//		}
//		else
//		{
//			mDying = false;
//			mDead = true;
//		}
//	}
//}
//
//bool Alien::IsDead()
//{
//	return mDead;
//}

void Alien::Destroy()
{
	smNumAliens--;
}

int Alien::GetNumAliens()
{
	return smNumAliens;
}
