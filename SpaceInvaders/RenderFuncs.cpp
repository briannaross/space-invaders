#include <memory>
#include "RenderFuncs.h"
#include "Structs.h"

void HandleEvent(GFXData &gfxData, SDL_Event &e, GObjs &gObjects, Resources &resources)
{
	// Handle queue events
	while (SDL_PollEvent(&e) != 0)
	{
		// User requests quit ('X' in top-right of window on Windows)
		if (e.type == SDL_QUIT)
			gObjects.gGameState = eNums::GameStates::END_GAME;

		else if (e.type == SDL_KEYUP)
		{
			// Main menu events
			if (gObjects.gGameState == eNums::GameStates::MAIN_MENU)
			{
				if (e.key.keysym.sym == SDLK_UP)
					gObjects.gMainMenuOption = ((gObjects.gMainMenuOption - 1) + 4) % 4;
				else if (e.key.keysym.sym == SDLK_DOWN)
					gObjects.gMainMenuOption = ((gObjects.gMainMenuOption + 1) + 4) % 4;
				else if (e.key.keysym.sym == SDLK_p)
				{
					gObjects.player.SetState(eNums::PlayerStates::REVIVING);
					gObjects.gGameState = eNums::GameStates::PRE_GAME;
					ResetGameObjects(gObjects);
				}
				else if (e.key.keysym.sym == SDLK_h)
					gObjects.gGameState = eNums::GameStates::HIGH_SCORES;
				else if (e.key.keysym.sym == SDLK_q)
					gObjects.gGameState = eNums::GameStates::OUTTRO;
				else if (e.key.keysym.sym == SDLK_RETURN)
				{
					switch (gObjects.gMainMenuOption)
					{
					case 0:
						ResetGameObjects(gObjects);
						gObjects.player.SetState(eNums::PlayerStates::REVIVING);
						gObjects.gGameState = eNums::GameStates::PRE_GAME;
						break;
					case 1:
						gObjects.gGameState = eNums::GameStates::HIGH_SCORES;
						break;
					case 2:
						gObjects.gGameState = eNums::GameStates::OPTIONS;
						break;
					case 3:
						gObjects.gGameState = eNums::GameStates::OUTTRO;
						break;
					}
				}
			}
			// In game events. Note that player events are handled below according to keystate
			else if (gObjects.gGameState == eNums::GameStates::PLAYING_GAME)
			{
				if (e.key.keysym.sym == SDLK_p)
					gObjects.gGameState = eNums::GameStates::PAUSED;
				else if (e.key.keysym.sym == SDLK_ESCAPE)
					gObjects.gGameState = eNums::GameStates::MAIN_MENU;
			}
			// Events while game paused
			else if (gObjects.gGameState == eNums::GameStates::PAUSED)
			{
				if (e.key.keysym.sym == SDLK_p)
					gObjects.gGameState = eNums::GameStates::PLAYING_GAME;
			}
			// Events in high scores screen
			else if (gObjects.gGameState == eNums::GameStates::HIGH_SCORES)
			{
				if (e.key.keysym.sym == SDLK_SPACE)
				{
					gObjects.gGameState = eNums::GameStates::MAIN_MENU;
				}
			}

		}

		// Handle window events
		gfxData.window.HandleEvent(e, gfxData.renderer);

		if ((e.type == SDL_WINDOWEVENT) && (SDL_WINDOWEVENT_SIZE_CHANGED))
			BGameObject::SetRatios(gfxData.window.GetSpriteRatio(), gfxData.window);
	}



	// Handle keystate events
	const Uint8 *state = SDL_GetKeyboardState(NULL);

	// Playing game
	if (gObjects.gGameState == eNums::GameStates::PLAYING_GAME)
	{
		if (state[SDL_SCANCODE_LEFT])
			gObjects.player.Move(eNums::Direction::LEFT);
		else if (state[SDL_SCANCODE_RIGHT])
			gObjects.player.Move(eNums::Direction::RIGHT);
		else if (state[SDL_SCANCODE_SPACE])
			gObjects.playerMissile.Fire(gObjects.player.GetRect(), resources.sndPlayerMissile);
	}
}

void RenderGame(GFXData &gfxData, Resources &resources, GObjs &gObjects)
{
	if (!gfxData.window.IsMinimized())
	{
		// Clear the window
		SDL_RenderClear(gfxData.renderer);

		// Render background
		RenderBackground(gfxData, resources, gObjects);
		if (gObjects.gGameState == eNums::GameStates::PLAYING_GAME)
		{
			// Render player missile
			gObjects.playerMissile.Render(gfxData.renderer, resources.background);
			// Render alien missiles
			gObjects.alienMissiles.Render(gfxData.renderer, resources.background);
			// Render mothership (if spawned)
			if (gObjects.motherShip.HasSpawned())
				gObjects.motherShip.Render(gfxData.renderer, resources.spritesheet);
		}
		// Render scoreboard
		RenderScoreboard(gfxData, resources, gObjects.player);
		// Render player
		gObjects.player.Render(gfxData.renderer, resources.spritesheet);
		// Render shields
		gObjects.shieldArray.Render(gfxData.renderer, resources.shield);
		// Render aliens
		gObjects.alienHoard.Render(gfxData.renderer, resources.spritesheet);

		// Render the changes above
		SDL_RenderPresent(gfxData.renderer);
	}
}

void RenderBackground(GFXData &gfxData, Resources &resources, GObjs &gObjects)
{
	// Render starfield
	SDL_RenderCopy(gfxData.renderer, resources.background.getTexture(), NULL, &gfxData.window.GetWindowRect());

	SDL_Rect dst;
	// Render grass
	dst.x = 0;
	dst.y = (int)(470 * gfxData.window.GetSpriteRatio());
	dst.h = (int)(resources.ground.getHeight() * gfxData.window.GetSpriteRatio());
	dst.w = (int)(INIT_SCREEN_WIDTH * gfxData.window.GetSpriteRatio());

	SDL_RenderCopy(gfxData.renderer, resources.ground.getTexture(), NULL, &dst);
}

void RenderScoreboard(GFXData &gfxData, Resources &resources, Player &player)
{
	SDL_Rect dst;
	SDL_Rect src;
	std::string str;

	/* Score */
	str = std::to_string(player.GetScore());
	resources.mainFont.setText(gfxData, str);

	dst.x = (int)(20 * gfxData.window.GetSpriteRatio());
	dst.y = (int)(20 * gfxData.window.GetSpriteRatio());
	dst.h = (int)(resources.mainFont.getHeight() * gfxData.window.GetSpriteRatio());
	dst.w = (int)(resources.mainFont.getWidth() * gfxData.window.GetSpriteRatio());

	SDL_RenderCopy(gfxData.renderer, resources.mainFont.getTexture(), NULL, &dst);

	/* Lives (Cannon sprite) */
	src.x = 0;
	src.y = 140;
	src.w = 32;
	src.h = 20;

	dst.x = (int)(200 * gfxData.window.GetSpriteRatio());
	dst.y = (int)(20 * gfxData.window.GetSpriteRatio());
	dst.h = (int)(20 * gfxData.window.GetSpriteRatio());
	dst.w = (int)(32 * gfxData.window.GetSpriteRatio());

	SDL_RenderCopy(gfxData.renderer, resources.spritesheet.getTexture(), &src, &dst);

	/* Lives (Number left) */
	str = std::to_string(player.GetLives());
	resources.mainFont.setText(gfxData, "x " + str);

	dst.x = (int)(240 * gfxData.window.GetSpriteRatio());
	dst.y = (int)(20 * gfxData.window.GetSpriteRatio());
	dst.h = (int)(resources.mainFont.getHeight() * gfxData.window.GetSpriteRatio());
	dst.w = (int)(resources.mainFont.getWidth() * gfxData.window.GetSpriteRatio());

	SDL_RenderCopy(gfxData.renderer, resources.mainFont.getTexture(), NULL, &dst);
}

void ResetGameObjects(GObjs &gObjects)
{
	gObjects.player.Reset();
	gObjects.motherShip.Reset();
	gObjects.alienHoard.Reset();
	gObjects.shieldArray.Reset();
	gObjects.playerMissile.Reset();
	gObjects.alienMissiles.Reset();
}

//void ReadHighScores(std::vector<HighScores> &highScores)
void ReadHighScores(GObjs &gObjects)
{
	SDL_RWops* file = SDL_RWFromFile("highscores.bin", "r");

	if (file == NULL)
	{
		std::cout << "Warning: Unable to open file! SDL_Error: " << SDL_GetError() << std::endl;
		file = SDL_RWFromFile("highscores.bin", "w");

		if (file != NULL)
		{
			std::cout << "New file created!" << std::endl;

			// Set default high score chart
			std::string initials = "@@@";
			for (int i = 0; i < 10; ++i)
			{
				for (int j = 0; j < 3; j++)
					initials[j]++; // Default initials go in sequence 'AAA', 'BBB', 'CCC', etc.

				snprintf(gObjects.highScores[i].initials, 4, "%s", initials.c_str());
				snprintf(gObjects.highScores[i].score, 10, "%s", "         ");
				snprintf(gObjects.highScores[i].score, 10, "%d", (10 - i) * 1000);
			}

			for (int i = 0; i < 10; ++i)
				SDL_RWwrite(file, &gObjects.highScores[i], sizeof(HighScores), 1);

			SDL_RWclose(file);
		}
		else
			std::cout << "Error: Unable to create file! SDL Error: " << SDL_GetError() << std::endl;
	}
	else
	{
		std::cout << "Reading file..." << std::endl;
		for (int i = 0; i < 10; ++i)
		{
			SDL_RWread(file, &gObjects.highScores[i], sizeof(HighScores), 1);
		}

		SDL_RWclose(file);
	}
}

void WriteHighScore()
{

}
