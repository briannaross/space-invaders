#pragma once

#include <vector>
#include <SDL.h>
#include "AlienMissile.h"
#include "PlayerMissile.h"
#include "Shield.h"
#include "Texture.h"

class ShieldArray
{
public:
	ShieldArray();
	~ShieldArray();
	void Reset();
	bool Hit(PlayerMissile &playerMissile);
	bool Hit(std::vector<AlienMissile>::iterator &alienMissile);
	void Render(SDL_Renderer *renderer, Texture &texture);

private:
	std::vector<std::vector<Shield>> shieldArray;
	std::vector< std::vector<Shield> >::iterator shield;
	std::vector<Shield>::iterator shieldSegment;

};
