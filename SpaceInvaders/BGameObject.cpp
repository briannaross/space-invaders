#include "BGameObject.h"
#include "Globals.h"


float BGameObject::smSpriteRatio = 1.0f;
int BGameObject::smXOffset = 0;
int BGameObject::smYOffset = 0;

BGameObject::BGameObject()
{

}

BGameObject::~BGameObject()
{
}

void BGameObject::SetRatios(float ratio, LF_Window &window)
{
	smSpriteRatio = ratio;

	smXOffset = window.getXBuffer();
	smYOffset = window.getYBuffer();
}

SDL_Rect BGameObject::GetRect()
{
	return mDst;
}

void BGameObject::StartDying()
{
	mDying = true;
	//mSrc.x = 64; // Set anim frame to first one with a score
	mSrc.x = (mAnimFrameOffset * 2); // Set to third frame
	mTimer->SetTicksPerSecond(12);
}

bool BGameObject::IsDying()
{
	return mDying;
}

void BGameObject::ProcessDying()
{
	mTimer->Update();
	if (mTimer->HasTicked())
	{
		if (mDeathTicksPassed < mTicksUntilDeath)
		{
			mDeathTicksPassed++;
			//mSrc.x += 32; // Set next death anim frame
			mSrc.x += mAnimFrameOffset;
		}
		else
		{
			mDying = false;
			mDead = true;
		}
	}
}

bool BGameObject::IsDead()
{
	return mDead;
}

void BGameObject::Render(SDL_Renderer *renderer, Texture &texture)
{
	SDL_Rect ratioDst;
	if (smSpriteRatio != 1.0f)
	{
		ratioDst.x = smXOffset + (int)((float)mDst.x * smSpriteRatio);
		ratioDst.y = smYOffset + (int)((float)mDst.y * smSpriteRatio);
		ratioDst.w = (int)((float)mDst.w * smSpriteRatio);
		ratioDst.h = (int)((float)mDst.h * smSpriteRatio);
		SDL_RenderCopy(renderer, texture.getTexture(), &mSrc, &ratioDst);
	}
	else
		SDL_RenderCopy(renderer, texture.getTexture(), &mSrc, &mDst);
}
