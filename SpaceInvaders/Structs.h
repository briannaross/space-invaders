#pragma once

#include <string>
#include <vector>
#include <SDL.h>
#include <SDL_mixer.h>
#include "Texture.h"
#include "Font.h"
#include "Player.h"
#include "AlienHoard.h"
#include "ShieldArray.h"
#include "AlienMissiles.h"
#include "PlayerMissile.h"
#include "Mothership.h"
#include "Window.h"

struct Resources
{
	// TODO Investigate using an unordered map so as to make it easier
	// to dynamically create and remove resources. Maybe a map for each
	// resource type, or maybe one big map for everything, don't know yet.
	Texture intro;
	Texture outtro;
	Texture mainmenu;
	Texture background;
	Texture ground;
	Texture spritesheet;
	Texture shield;

	Font mainFont;

	Mix_Chunk *sndPlayerMissile;
	Mix_Chunk *sndAlienMissile;
	Mix_Chunk *sndExplosion;
	Mix_Chunk *sndDeath;
	Mix_Chunk *sndMothership;
	int sndMothershipChannel;

};

struct HighScores
{
	char initials[4];
	char score[10];
	//int score;
};

struct GObjs
{
	// Init player and computer hands
	Player player = Player("Human");

	// Init alien hoard
	AlienHoard alienHoard;

	// Init shield array
	ShieldArray shieldArray;

	// Init alien missiles
	AlienMissiles alienMissiles;

	// Prepare player missile
	PlayerMissile playerMissile;

	// Prepare mothership
	Mothership motherShip;

	// High score chart
	HighScores highScores[10];

	// Current game state
	eNums::GameStates gGameState = eNums::GameStates::INTRO;

	// TODO Consider making this an enum
	int gMainMenuOption = 0;
};
