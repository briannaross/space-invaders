#pragma once

#include <vector>
#include <SDL.h>
#include "Alien.h"
#include "AlienMissiles.h"
#include "PlayerMissile.h"
#include "Timer.h"
#include "Player.h"
#include "Globals.h"

class AlienHoard
{
public:
	AlienHoard();
	~AlienHoard();
	void Move();
	bool CollidedWithEdge();
	void Reset();
	void Render(SDL_Renderer *renderer, Texture &texture);
	void FireMissile(AlienMissiles &alienMissiles);
	int CheckForHit(PlayerMissile &playerMissile);
	void ProcessDeadAndDyingAliens();
	bool Landed();

private:
	void SetMovementInterval();
		
	std::vector<Alien> hoard;
	std::vector<Alien>::iterator itAlien;
	Timer *timer;
};
