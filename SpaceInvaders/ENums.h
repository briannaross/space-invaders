#pragma once

namespace eNums
{
	enum GameStates
	{
		INTRO,
		MAIN_MENU,
		PRE_GAME,
		PLAYING_GAME,
		PAUSED,
		PLAYER_LOST_LIFE,
		END_GAME,
		POST_GAME,
		HIGH_SCORES,
		OPTIONS,
		OUTTRO
	};

	enum PlayerStates
	{
		REVIVING,
		ALIVE,
		DYING,
		DEAD
	};

	enum Direction
	{
		LEFT = -1,
		RIGHT = 1
	};
}
