#include "AlienMissile.h"

int AlienMissile::smNumMissiles = 0;

AlienMissile::AlienMissile(SDL_Point &origin)
{
	mYPos = (double)origin.y;

	mPos.x = origin.x;
	mPos.y = origin.y;

	mDst.x = origin.x;
	mDst.y = origin.y;
	mDst.w = 4;
	mDst.h = 18;

	mSrc.x = 12;
	mSrc.y = 161;
	mSrc.w = 4;
	mSrc.h = 18;

	smNumMissiles++;
	mVelocity = 1;
}

AlienMissile::~AlienMissile()
{
}

void AlienMissile::Move()
{
	mYPos += (MISSILE_SPEED * mVelocity);
	mPos.y = (int)mYPos;
	mDst.y = (int)mYPos;
}

bool AlienMissile::Hit(SDL_Rect alien)
{
	SDL_Point missileCentre;
	missileCentre.x = mDst.x + mDst.w / 2;
	missileCentre.y = mDst.y + mDst.h / 2;

	if ((missileCentre.x > alien.x) && (missileCentre.x < alien.x + alien.w) &&
		(missileCentre.y > alien.y) && (missileCentre.y < alien.y + alien.h))
	{
		return true;
	}

	return false;
}

bool AlienMissile::LeftPlayfield()
{
	if (mDst.y > INIT_SCREEN_HEIGHT)
		return true;

	return false;
}

void AlienMissile::Destroy()
{
	smNumMissiles--;
}

int AlienMissile::GetNumMissiles()
{
	return smNumMissiles;
}
