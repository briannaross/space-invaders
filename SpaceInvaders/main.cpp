#include <stdlib.h>
#include <time.h>
#include <string>
#include "SDLWrapper.h"
#include "Globals.h"
#include "ENums.h"
#include "GFXData.h"
#include "Structs.h"
#include "GameStateFuncs.h"
#include "Texture.h"
#include "Font.h"

bool InitResources(GFXData &gfxData, Resources &resources);

int main(int argc, char* args[])
{
	GFXData gfxData;

	// Start up GFX and create window
	if (InitGFX(gfxData)) {
		/**********************/
		/*** INITIALISATION ***/
		/**********************/

		// Set up resources. If any load fails exit program
		Resources resources;
		if (!InitResources(gfxData, resources)) {
			exit(1);
		}

		// SDL event handler
		SDL_Event e;

		// All the game objects
		GObjs gObjects;

		// Read in the high scores from the high score chart (or create one if non-existent).
		// Do this here because you don't know whether it will be accessed from main menu
		// or after attaining a high score.
		ReadHighScores(gObjects);


		// Use for debugging - go straight to whatever state
		//gObjects.gGameState = eNums::GameStates::PRE_GAME;


		// State handler
		while(gObjects.gGameState != eNums::GameStates::END_GAME) {
			switch (gObjects.gGameState) {
				case eNums::GameStates::INTRO:				GameStateIntro(gfxData, gObjects, resources);				break;
				case eNums::GameStates::MAIN_MENU:			GameStateMainMenu(e, gfxData, gObjects, resources);			break;
				case eNums::GameStates::PRE_GAME:			GameStatePrePlay(e, gfxData, gObjects, resources);			break;
				case eNums::GameStates::PLAYING_GAME:		GameStatePlayingGame(e, gfxData, gObjects, resources);		break;
				case eNums::GameStates::PAUSED:																			break;
				case eNums::GameStates::PLAYER_LOST_LIFE:	GameStatePlayerLostLife(e, gfxData, gObjects, resources);	break;
				case eNums::GameStates::END_GAME:																		break;
				case eNums::GameStates::POST_GAME:																		break;
				case eNums::GameStates::HIGH_SCORES:		GameStateHighScores(e, gfxData, gObjects, resources);		break;
				case eNums::GameStates::OPTIONS:																		break;
				case eNums::GameStates::OUTTRO:				GameStateOuttro(gfxData, gObjects, resources);				break;
				default:																								break;
			}
		}

		// Free resources and close GFX
		Mix_FreeChunk(resources.sndAlienMissile);
		Mix_FreeChunk(resources.sndDeath);
		Mix_FreeChunk(resources.sndExplosion);
		Mix_FreeChunk(resources.sndMothership);
		Mix_FreeChunk(resources.sndPlayerMissile);

		ShutdownGFX(gfxData);
	}

	return 0;
}

bool InitResources(GFXData &gfxData, Resources &resources)
{
	/*** Textures ***/
	if (!(resources.intro.LoadFromFile(gfxData, "intro.png"))) return false;
	if (!(resources.outtro.LoadFromFile(gfxData, "outtro.png"))) return false;
	if (!(resources.mainmenu.LoadFromFile(gfxData, "mainmenu.png"))) return false;
	if (!(resources.background.LoadFromFile(gfxData, "background.png"))) return false;
	if (!(resources.spritesheet.LoadFromFile(gfxData, "spaceinvaders-spritesheet.png"))) return false;
	if (!(resources.shield.LoadFromFile(gfxData, "shield.png"))) return false;
	if (!(resources.ground.LoadFromFile(gfxData, "grass.png")))	return false;

	/*** Fonts ***/
	SDL_Color white = { 0xFF, 0xFF, 0xFF };
	if (!(resources.mainFont.setFont(gfxData, "ARCADECLASSIC.TTF"))) return false;
	if (!(resources.mainFont.setText(gfxData, "{{Placeholder}}"))) return false;
	if (!(resources.mainFont.setColour(gfxData, white))) return false;

	/*** Audio ***/
	resources.sndAlienMissile = Mix_LoadWAV("AlienMissile.wav");
	if (resources.sndAlienMissile != NULL) {
		Mix_VolumeChunk(resources.sndAlienMissile, 32);
	}

	resources.sndPlayerMissile = Mix_LoadWAV("PlayerMissile.wav");
	resources.sndExplosion = Mix_LoadWAV("Explosion.wav");
	resources.sndDeath = Mix_LoadWAV("Death.wav");
	resources.sndMothership = Mix_LoadWAV("Mothership.wav");

	return true;
}
