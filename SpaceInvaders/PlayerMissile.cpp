#include "PlayerMissile.h"

PlayerMissile::PlayerMissile()
{
	ResetPosition();

	mDst.w = 4;
	mDst.h = 18;

	mVelocity = -1;

	mSpawned = false;

	mSrc.x = 12;
	mSrc.y = 161;
	mSrc.w = 4;
	mSrc.h = 18;
}


PlayerMissile::~PlayerMissile()
{
}

bool PlayerMissile::Fire(SDL_Rect &spawnRect, Mix_Chunk *snd)
{
	if (mSpawned == false)
	{
		SDL_Point origin;
		origin.x = spawnRect.x + (spawnRect.w / 2);
		origin.y = spawnRect.y;

		mYPos = (double)origin.y;
		mPos.x = origin.x;
		mPos.y = origin.y;
		mDst.x = origin.x;
		mDst.y = origin.y;

		mSpawned = true;

		// Play fire missile sound whenever missile is fired
		Mix_PlayChannel(-1, snd, 0);

		return true;
	}

	return false;
}

void PlayerMissile::Move()
{
	mYPos += (MISSILE_SPEED * mVelocity);
	mPos.y = (int)mYPos;
	mDst.y = (int)mYPos;
}

bool PlayerMissile::Hit(SDL_Rect target)
{
	SDL_Point missileCentre;
	missileCentre.x = mDst.x + mDst.w / 2;
	missileCentre.y = mDst.y + mDst.h / 2;

	if ((missileCentre.x > target.x) && (missileCentre.x < target.x + target.w) &&
		(missileCentre.y > target.y) && (missileCentre.y < target.y + target.h))
	{
		mSpawned = false;
		ResetPosition();
		return true;
	}

	return false;
}

bool PlayerMissile::CheckIfLeftPlayfield()
{
	if (HasSpawned() == true)
		if (mDst.y + mDst.h < 0)
		{
			mSpawned = false;
			ResetPosition();
			return true;
		}

	return false;
}

bool PlayerMissile::HasSpawned()
{
	return mSpawned;
}

void PlayerMissile::Reset()
{
	ResetPosition();
}

void PlayerMissile::ResetPosition()
{
	mYPos = -100.0f;

	mPos.x = -100;
	mPos.y = -100;

	mDst.x = -100;
	mDst.y = -100;
}
