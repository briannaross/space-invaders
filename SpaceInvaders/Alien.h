#pragma once

#include <SDL.h>
#include "BGameObject.h"
#include "Texture.h"

class Alien : public BGameObject
{
public:
	Alien(int row, int column);
	~Alien();
	int GetHitPoints();
	void MoveAcross();
	void MoveDown();
	void Animate();
	bool CollidedWithEdge();
	//void StartDying();
	//bool IsDying();
	//void ProcessDying();
	//bool IsDead();
	void Destroy();
	static int GetNumAliens();

private:

	bool mMoveLeft = true;
	bool mLastTurnMovedDown = false;
	//bool mDying;
	//bool mDead;
	//int mTicksUntilDeath;
	//int mDeathTicksPassed;
	int mSpriteFrame;
	int mHitPoints;
	SDL_Point mSpritePositions[2];
	static int smNumAliens;
};
