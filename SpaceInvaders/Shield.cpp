#include "Shield.h"
#include "Globals.h"

Shield::Shield(int row, int column, SDL_Point pos)
{
	mRow = row;
	mColumn = column;
	mPos = pos;
	mHitPoints = SHIELD_HIT_POINTS;

	// Initial frame on spritesheet 
	//mSrc.x = 0;
	//mSrc.y = 0;
	//mSrc.x = ((mHitPoints - 1) * 32) + (mColumn * 8);
	//mSrc.y = 12 * mRow;
	UpdateSpriteSourcePos();
	mSrc.h = 18;
	mSrc.w = 8;

	// Rendering position
	mDst.x = mPos.x + (column * 8);
	mDst.y = mPos.y + (row * 12);
	mDst.h = 18;
	mDst.w = 8;

}

Shield::~Shield()
{
}

SDL_Point Shield::GetPos()
{
	return mPos;
}

SDL_Rect Shield::GetRect()
{
	return mDst;
}

int Shield::GetRow()
{
	return mRow;
}

int Shield::GetColumn()
{
	return mColumn;
}

void Shield::Damage()
{
	mHitPoints--;
	UpdateSpriteSourcePos();
}

void Shield::UpdateSpriteSourcePos()
{
	mSrc.x = ((mHitPoints - 1) * 32) + (mColumn * 8);
	mSrc.y = 12 * mRow;
}

bool Shield::Destroyed()
{
	if (mHitPoints == 0)
		return true;

	return false;
}
