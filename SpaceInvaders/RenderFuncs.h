#pragma once

#include <iostream>
#include <vector>
#include "Structs.h"

void HandleEvent(GFXData &gfxData, SDL_Event& e, GObjs &gObjects, Resources &resources);
void RenderGame(GFXData &gfxData, Resources &resources, GObjs &gObjects);
void RenderBackground(GFXData &gfxData, Resources &resources, GObjs &gObjects);
void RenderScoreboard(GFXData &gfxData, Resources &resources, Player &player);
void ResetGameObjects(GObjs &gObjects);
//void ReadHighScores(std::vector<HighScores> &highScores);
void ReadHighScores(GObjs &gObjects);
void WriteHighScore();
